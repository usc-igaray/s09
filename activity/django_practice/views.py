from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

# Create your views here.
def logout(request):
    user = User.objects.get(username="Joosh")
    print('here')
    user.is_active = False
    user.save()
    return redirect('login')

def index(request):
    user = User.objects.get(username="Joosh")
    context = {"is_active": False}

    if user is not None:
        context["is_active"] = user.is_active  
        if user.is_active == False:
            redirect('django_practice/login.html', context)
        else:
            context["username"] = user.username
            
    return render(request, 'django_practice/index.html', context)

def login(request):
    context = {"is_active": False}
    users = User.objects.all()
    user = None
    
    for userItem in users:
        if userItem.username == "Joosh":
            user = userItem
            break
    
    if user is not None:
        user.is_active = True
        user.save()
        context["is_active"] = True
        return redirect('index')
    else:
        print('here')
    return render(request, 'django_practice/login.html', context)

def change_password(request):
    user = authenticate(username="Joosh", password="Password123")
    is_valid = False
    if user is not None:
        is_valid = True
        user.set_password("NotAPassword")
        user.save()
    
    return render(request, 'django_practice/change_password.html', {"is_valid": is_valid})

def register(request):
    users = User.objects.all()
    user = None
    
    context = {
        "is_duplicate": True
    }
    
    for userItem in users:
        if userItem.username == "Joosh":
            user = userItem
            break
    
    if (user is not None):
        context["is_duplicate"] = True
    else:
        user = User.objects.create(
            username="Joosh",
            email="joosh@gmail.com"
        )
        user.set_password("Password123")
        user.save()
        context["username"] = "Joosh"
        context["is_duplicate"] = False
        
    return render(request, 'django_practice/register.html', context)
